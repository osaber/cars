package hu.sabero.examplecode.cars.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;

import hu.sabero.examplecode.cars.R;
import hu.sabero.examplecode.cars.presenter.CarModelDetailPresenter;
import hu.sabero.examplecode.cars.presenter.CarModelDetailPresenterImpl;
import hu.sabero.examplecode.cars.util.ImageCache;
import hu.sabero.examplecode.cars.util.ImageFetcher;

/**
 * An activity representing a single car model detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link CarsActivity}.
 */
public class CarModelDetailActivity extends AppCompatActivity implements CarModelDetailView {

    private CarModelDetailPresenter presenter;
    private ImageFetcher imageFetcher;
    private static final String IMAGE_CACHE_DIR = "images";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new CarModelDetailPresenterImpl(this);
        setContentView(R.layout.activity_car_model_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        imageFetcher = new ImageFetcher(this, longest);
        imageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);
        imageFetcher.setImageFadeIn(false);

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable("presenter", presenter);
            CarModelDetailFragment fragment = new CarModelDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.car_model_detail_container, fragment)
                    .commit();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            navigateUpTo(new Intent(this, CarsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public CarModelDetailPresenter getPresenter() {
        return presenter;
    }

    @Override
    public Context getContext() {
        return this;
    }

    /**
     * Called by the CarModelDetailFragment to load image via the one ImageFetcher
     */
    public ImageFetcher getImageFetcher() {
        return imageFetcher;
    }
}
