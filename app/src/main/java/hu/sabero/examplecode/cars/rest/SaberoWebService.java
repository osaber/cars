package hu.sabero.examplecode.cars.rest;

import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;

import hu.sabero.examplecode.cars.presenter.Presenter;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by saber on 2016. 07. 11..
 */
public class SaberoWebService extends AsyncTask<String, Void, String> {

    private Presenter presenter;
    private Context context;

    public SaberoWebService(Context context, Presenter presenter){
        super();
        this.context = context;
        this.presenter = presenter;
    }

    public static String getResponse(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    @Override
    protected String doInBackground(String... urls) {

        try {
            return getResponse(urls[0]);
        } catch (IOException e) {
            return e.getLocalizedMessage();
        }
    }


    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
        presenter.callback(result);
    }
}
