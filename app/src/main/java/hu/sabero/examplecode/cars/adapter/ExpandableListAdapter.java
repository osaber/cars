package hu.sabero.examplecode.cars.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.activeandroid.query.Select;

import java.util.HashMap;
import java.util.List;

import hu.sabero.examplecode.cars.R;
import hu.sabero.examplecode.cars.model.CarMake;
import hu.sabero.examplecode.cars.model.CarModel;
import hu.sabero.examplecode.cars.util.ImageFetcher;

/**
 * A Class for viewing car make and car model lists on Expandable List
 * Created by saber on 2016. 07. 11..
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<CarModel>> _listDataChild;
    private ImageFetcher _imageFetcher;

    /**
     * Constructor
     * @param context: a Context object to initialize _context variable
     * @param listDataHeader: a list of String objects to initialize _listDataHeader variable
     * @param listChildData: a map of String-TeamMember objects to initialize _listDataChild  variable
     */
    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<CarModel>> listChildData, ImageFetcher imageFetcher) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this._imageFetcher = imageFetcher;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        CarModel carModel = (CarModel) getChild(groupPosition, childPosition);

        final String name = carModel.getName();
        final String price = "£" + Double.toString(carModel.getPrice());
        LayoutInflater infalInflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = infalInflater.inflate(R.layout.car_list_model, null);

        TextView carModelNameView = (TextView) convertView
                .findViewById(R.id.car_model_name);
        carModelNameView.setText(name);

        TextView carModelPriceView = (TextView) convertView
                .findViewById(R.id.car_model_price);
        carModelPriceView.setText(price);

        ImageView carModelImage = (ImageView) convertView.findViewById(R.id.car_model_image);
        /*byte[] image = carModel.getImage();
        carModelImage.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.length));*/
        // Finally load the image asynchronously into the ImageView, this also takes care of
        // setting a placeholder image while the background thread runs
        _imageFetcher.loadImage(carModel.getImageURL(), carModelImage);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String groupName = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_list_make, null);
        }

        TextView carMakeName = (TextView) convertView
                .findViewById(R.id.car_make_name);
        carMakeName.setTypeface(null, Typeface.BOLD);
        carMakeName.setText(groupName);

        ImageView carMakeLogo = (ImageView) convertView.findViewById(R.id.car_make_logo);
        _imageFetcher.loadImage(getCarMakeLogoURL(groupName), carMakeLogo);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public String getCarMakeLogoURL(String name){
        CarMake carMake = new Select().from(CarMake.class).where("name = ?", name).executeSingle();
        if(carMake != null){
            return carMake.getLogoURL();
        }else{
            return "";
        }
    }


}
