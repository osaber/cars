package hu.sabero.examplecode.cars.presenter;

import android.os.Parcelable;
import hu.sabero.examplecode.cars.model.CarModel;

/**
 * Presenter/ controller interface which controle the "CarModelDetailActivity" bihaviours
 * Created by saber on 2016. 08. 01..
 */
public interface CarModelDetailPresenter extends Parcelable {

    /**
     * Method to load selected car model from the device database and gives it's reference to "selectedCarModel"
     */
    void prepareCarModelFromSQLiteDb();

    /**
     * Method to query the selected car model
     * @return selectedCarModel: CarModel object
     */
    CarModel getSelectedCarModel();
}
