package hu.sabero.examplecode.cars.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.activeandroid.query.Select;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import hu.sabero.examplecode.cars.CarsApplication;
import hu.sabero.examplecode.cars.R;
import hu.sabero.examplecode.cars.model.CarMake;
import hu.sabero.examplecode.cars.model.CarModel;
import hu.sabero.examplecode.cars.rest.SaberoWebService;
import hu.sabero.examplecode.cars.view.CarsActivity;
import hu.sabero.examplecode.cars.view.MainView;

/**
 * Created by saber on 2016. 07. 11..
 */
public class SplashPresenterImpl implements SplashPresenter {


    private MainView splashView;
    private JSONArray saberoWebServiceResponse;

    public SplashPresenterImpl(MainView splashView) {
        this.splashView = splashView;
    }

    @Override
    public void checkNetwork() {
        ConnectivityManager connectivityManager = (ConnectivityManager) splashView.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            CarsApplication.setOfflineMode(false);
        }else{
            Toast.makeText(splashView.getContext(), splashView.getContext().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
            CarsApplication.setOfflineMode(true);
        }
    }

    @Override
    public void callSaberoWebService(SplashPresenter presenter) {
        String url = CarsApplication.getWebServiceUrl();
        SaberoWebService saberoWebService = new SaberoWebService(splashView.getContext(), presenter);
        saberoWebService.execute(url);
    }

    @Override
    public void setSaberoWebServiceResponse(JSONArray response) {
        this.saberoWebServiceResponse = response;
        Log.d("TAG", response.toString());
    }

    @Override
    public JSONArray getSaberoWebServiceResponse() {
        return saberoWebServiceResponse;
    }

    @Override
    public void saveResponseToSQLiteDb() {

        JSONArray response = getSaberoWebServiceResponse();
        for (int i = 0; i < response.length(); i++) {
            JSONObject carMakeObject = response.optJSONObject(i);
            if(!carMakeObject.optString("make").isEmpty()){
                String carMakeName = carMakeObject.optString("make");
                String logoURL = carMakeObject.optString("logoURL");
                CarMake carMake = new Select().from(CarMake.class).where("name = ?", carMakeName).executeSingle();

                if(carMake == null) {
                    carMake = new CarMake();
                    carMake.setName(carMakeName);
                    carMake.setLogoURL(logoURL);
                    carMake.save();
                }else {
                    carMake.update(carMakeName, logoURL);
                }

                List<CarModel> carModelList = new ArrayList<CarModel>();
                JSONArray carModels = carMakeObject.optJSONArray("models");

                for (int j = 0; j < carModels.length(); j++){
                    JSONObject carModelObject = carModels.optJSONObject(j);
                    CarModel carModel = new CarModel();

                    carModel.setName(carModelObject.optString("name"));
                    carModel.setFormat(carModelObject.optString("format"));
                    carModel.setPosition(carModelObject.optString("position"));
                    carModel.setEngine(carModelObject.optString("engine"));
                    carModel.setSize(carModelObject.optDouble("size"));
                    carModel.setZero_62mph(carModelObject.optDouble("zero_62mph"));
                    carModel.setPower(carModelObject.optInt("power"));
                    carModel.setTopSpeed(carModelObject.optInt("topSpeed"));
                    carModel.setPrice(carModelObject.optDouble("price"));
                    carModel.setImageURL(carModelObject.optString("imageURL"));

                    carModel.setCarMake(carMake);
                    carModelList.add(carModel);

                    CarModel carModelFromDb = new Select().from(CarModel.class).where("name = ?", carModel.getName()).executeSingle();
                    if(carModelFromDb == null) {
                        carModel.save();
                    }else {
                        carModelFromDb.update(carModel);
                    }
                }
            }
        }
    }

    @Override
    public void navigateToCarsView(Context context) {
        Intent intent = new Intent(context, CarsActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void callback(String result) {
        try {
            setSaberoWebServiceResponse(new JSONArray(result));
            saveResponseToSQLiteDb();
            navigateToCarsView(splashView.getContext());
            ((Activity)splashView.getContext()).finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

