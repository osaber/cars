package hu.sabero.examplecode.cars.view;

import android.content.Context;

/**
 * Created by saber on 2016. 07. 11..
 */
public interface MainView {
    Context getContext();
}
