package hu.sabero.examplecode.cars.presenter;

import android.content.Context;

import org.json.JSONArray;

/**
 * Presenter/ controller interface which controle the "SplashActivity" bihaviors
 * Created by saber on 2016. 07. 11..
 */
public interface SplashPresenter extends Presenter {
    /**
     * Method to checking network connection and setup "TeamsApplication.offlineMode" state to "true" if ther is no network connection else to "false"
     */
    void checkNetwork();

    /**
     * Method to call webservice
     * @param presenter
     */
    void callSaberoWebService(SplashPresenter presenter);

    /**
     * Method to set the called webservice response
     * @param response: JSONArray object
     */
    void setSaberoWebServiceResponse(JSONArray response);

    /**
     * Method to query the called webservice response
     * @return JSONArray object
     */
    JSONArray getSaberoWebServiceResponse();

    /**
     * Method to parse the JSONArray response and save data records to the SQLite database of the device
     */
    void saveResponseToSQLiteDb();

    /**
     * Method to navigate to team list result screen
     * @param context
     */
    void navigateToCarsView(Context context);

}
