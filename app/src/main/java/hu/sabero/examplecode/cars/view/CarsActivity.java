package hu.sabero.examplecode.cars.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;


import hu.sabero.examplecode.cars.R;
import hu.sabero.examplecode.cars.presenter.CarsPresenter;
import hu.sabero.examplecode.cars.presenter.CarsPresenterImpl;
import hu.sabero.examplecode.cars.util.ImageCache;
import hu.sabero.examplecode.cars.util.ImageFetcher;

/**
 * An activity representing a list of car models grouped by their car make. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link hu.sabero.examplecode.cars.view.CarModelDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */

/**
 * Created by saber on 2016. 07. 11..
 */
public class CarsActivity extends AppCompatActivity implements CarsView {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private CarsPresenter presenter;
    private ImageFetcher imageFetcher;
    private static final String IMAGE_CACHE_DIR = "thumbs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        imageFetcher = new ImageFetcher(this, getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size));
        //imageFetcher.setLoadingImage(R.drawable.empty_photo);
        imageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);

        ExpandableListView expandableListView = (ExpandableListView)findViewById(R.id.team_member_list);
        assert expandableListView != null;
        presenter = new CarsPresenterImpl(this, expandableListView);
        presenter.prepareCarListFromSQLiteDb();
        presenter.showCarListOnScreen();
        if (findViewById(R.id.team_member_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public ImageFetcher getImageFetcher() {
        return imageFetcher;
    }
}
